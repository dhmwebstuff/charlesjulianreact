<!doctype html>
<html lang="en">
	<head profile="http://www.w3.org/2005/10/profile">
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Charles Julian Jr - Web Developer</title>
		<!-- <link rel="stylesheet" href="<?php //bloginfo( stylesheet_url ); ?>" /> -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		<link rel="stylesheet" href="http://charlesjulianjr.com/staging/charlesjulian/src/styles/styles.min.css">
		<link rel="stylesheet" href="http://charlesjulianjr.com/staging/charlesjulian/src/styles/purchaseTableStyles.min.css">
		<link rel='shortcut icon' href="//charlesjulianjr.com/favicon.ico" type="image/x-icon" />
	</head>
	<body>
		<div class="app"></div>
		<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="http://charlesjulianjr.com/staging/charlesjulian/dist/bundle.js"></script>
	</body>
</html>
