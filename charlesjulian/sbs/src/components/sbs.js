import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

class SBS extends Component {
  constructor(props) {
    super(props);

    this.state = {
      clearDOM: false
    };
  }

  componentDidMount() {
    $('[data-toggle="tooltip"]').tooltip();
    console.log("SERVER 123");
  }

  clearCJDOM() {
    console.log("CLEAR CJ DOM456");
  }

  renderCheckoutFormTheShop() {
    return (
      <form
        target="paypal"
        action="https://www.paypal.com/cgi-bin/webscr"
        method="post"
        key="theShop-form"
      >
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="FJ5XFFBJRXVK8" />
        <input
          type="image"
          src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif"
          name="submit"
          alt="PayPal - The safer, easier way to pay online!"
        />
      </form>
    );
  }

  renderCheckoutFormMDBC() {
    console.log("checkout form");
    return (
      <form
        target="paypal"
        action="https://www.paypal.com/cgi-bin/webscr"
        method="post"
        key="mdbc-form"
      >
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="2DWG3S2HEQT3S" />
        <input
          type="image"
          src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif"
          name="submit"
          alt="PayPal - The safer, easier way to pay online!"
        />
      </form>
    );
  }

  renderIntroSection() {
    return (
      <div className="intro">
        <h1>Small Business Solutions</h1>
        <p>
          I provide website solutions to small businesses and entrepreneurs for
          affordable prices and timely delivery. I offer two different website
          packages for your budget. I am able to get website out quickly because
          I give the business owner a choice of templates to choose from which
          allow me to speed up the development process. While templates are
          used, each site has a unique experience.
        </p>
        {/* I have three different website
        packages for your budget. I am able to get website out quickly because
        I give the business owner a choice of templates to choose from which
        allow me to speed up the development process. While templates are
        used, each site has a unique experience. */}
        <p>
          Are you just starting out and want a website to show who you are?{" "}
          <a href="#packages">
            <strong>The MBDC is for you.</strong>
          </a>
        </p>
        <p>
          Do you have goods or services to sell and want the ability to manage
          inventory?{" "}
          <a href="#packages">
            <strong>The Shop is for you.</strong>
          </a>
        </p>
        {/* <p>
          Do you want a fully custom site or need a mobile application or next
          technology services such as chatbots? The [INSERT PACKAGE NAME] is
          what your looking for.
        </p> */}
        <p>Have questions? Check the FAQ below</p>
        <h2>FAQ</h2>
        <h3>Why does a entrepreneur or buisness need a website?</h3>
        <p>
          A website is vital to entrepreneurs and small business owners in
          today's economy. E-commerce platforms offer an easier and more
          affordable way to sell your goods or services without the hassle of
          worrying about security and customer support.
        </p>
        <h3>What if I am just starting and don't have anything to sell?</h3>
        <p>
          Having a web presence to showcase your talents is still necessary if
          you are serious about your craft. There's no problem in starting off
          slow. If you are not ready to sell, you can still bring traffic to
          your site through social media and keep them engage by encouraging
          them to signup for a newletter or updates on your projects or by a
          contact form. If you are just starting out, your website should be an
          extension of your business card. A person should be able to find out
          who you are, what you do, and how to contact you.
        </p>
        <h3>
          I have products or services to sell and I need a website. I've heard
          about E-commerce platforms like Shopify, Big Cartel, Squarespace, etc.
          but I'm not sure if I should use one of these services or make a
          custom site. What should I do?
        </h3>
        <p>
          Unless you are a well established brick and mortar business or have a
          large budget, I would stay away from a building an e-commerce website
          from the ground up. Building a e-commerce website from scratch can be
          a lenghty and expensive process. A good web developer's price will
          start in the low thousands and can go into the hundreds of thousands
          depending on the complexity of the project. You will also have to pay
          for an SSL certificate to ensure your transactions are encrypted and
          these certificates are quite expensive. If you are interested in this
          type of project, The [INSERT PACKAGE NAME] is for you.
        </p>
        <p>
          E-Commerce platforms are a better solution for business owners with a
          smaller budget and smaller operations. I have found Squarespace to be
          a more than capable E-commerce platform for entrepreneurs and small
          business owners because of its ease of use. They offer good templates
          that can be easily customized to your tastes to make it your own.
        </p>
        <h3>Ok I'm ready to get my website made. What do I need to do?</h3>
        <p>
          Welcome to the big leagues. But before you step on the field, you will
          need the proper equipment. Here's a checklist of what you'll need for
          each package:
        </p>
        <div className="checklist">
          <strong>The MBDC</strong>
          <ul>
            <li>A Domain name (TheNameOfYourWebsite.com)</li>
            <li>
              A Hosting Service{" "}
              <a href="https://aws.amazon.com/websites/">AWS</a> (Amazon Web
              Services) or{" "}
              <a href="https://www.digitalocean.com/products/droplets/">
                Digital Ocean{" "}
              </a>
              preferred with login credentials)
            </li>
            <li>Images and Text for slideshow (5 max) or a video loop</li>
            <li>Text for Bio/About Section</li>
            <li>Account for social media (Twitter, Facebook, Instagram)</li>
            <li>List of Services Provided</li>
            <li>Account for email marketing/lead generation</li>
            <li>Email address for contact form</li>
          </ul>
        </div>
        <div className="checklist">
          <strong>The Shop</strong>
          <ul>
            <li>A Domain name (TheNameOfYourWebsite.com)</li>
            <li>Stripe or Paypal account</li>
            <li>
              Squarespace account minimum Website Business Account (will need
              account information to set up your website)
            </li>
            <li>Images and Text for slideshow (5 max) or a video loop</li>
            <li>Text for Bio/About Section</li>
            <li>Account for social media (Twitter, Facebook, Instagram)</li>
            <li>List of Services Provided</li>
            <li>Account for email marketing/lead generation</li>
            <li>Email address for contact form</li>
            <li>
              Product Information (price, sale price, description, length,
              width, height and weight of shipping box (if applicable))
            </li>
            <li>Product images (10 products max, 2 images per product)</li>
            <li>
              List of copy and images and copy and images for each page (5 max)
            </li>
          </ul>
        </div>
        <div>
          Here are some royalty free sites that you can find images and video
          for:
          <li>
            Free stock images:{" "}
            <a href="https://unsplash.com/" target="_blank">
              Unsplash
            </a>
          </li>
          <li>
            Free stock videos:{" "}
            <a href="https://www.videvo.net/" target="_blank">
              Videvo
            </a>
          </li>
        </div>
        <div className="nextSteps">
          <h3>Ok I'm ready to have my website made what do I do next?</h3>
          <p>
            <strong>
              Make sure you have each item on the checklist for the package you
              are choosing.
            </strong>
            <br />
            7 day Turnaround is not guaranteed unless all checklist items at the
            time of deposit.
          </p>
        </div>

        <a name="packages">
          <h2>Small Business Packages</h2>
        </a>
      </div>
    );
  }

  renderPricingTables() {
    return (
      <div className="page-container">
        <div className="pricing-table-wrapper">
          <div className="pricing-table">
            <div className="pricing-table-header">
              <h2>The MDBC</h2>
              <h3>$300</h3>
            </div>
            <div className="pricing-table-space" />
            <div className="pricing-table-text">
              <p>
                <strong>The Modern Day Business Card.</strong>
              </p>
              <p>
                A solution for the starting entrepreneur. Your website will give
                a concise vision of your buisness while also giving options for
                client leads and feedback.
              </p>
            </div>
            <div className="pricing-table-features">
              <p>
                <strong>1</strong> Single Page Website
              </p>
              <p>
                <strong>1</strong> Image Slideshow/Video Loop
              </p>
              <p>
                <strong>1</strong> Bio Section
              </p>
              <p>
                <strong>1</strong> Socia Media Integration
              </p>
              <p>
                <strong>1</strong> Services/Features Section
              </p>
              <p>
                <strong>1</strong> Newsletter Signup Form To Your Email Campaign
                Provider
              </p>
              <p>
                <strong>1</strong> Contact Form
              </p>
              <p>
                <strong>7 days</strong> Turnaround Time
                <i
                  data-toggle="tooltip"
                  title="Turnaround time is dependent upon the client having all the necessary assets (hi-res images, text for website, email address for contact form, social media accounts)"
                  className="far fa-question-circle"
                />
              </p>
              <a href="#" className="example-link">
                example
              </a>
            </div>
            <div className="pricing-table-sign-up">
              {this.renderCheckoutFormMDBC()}
            </div>
          </div>

          <div className="pricing-table pricing-table-highlighted">
            <div className="pricing-table-header">
              <h2>The Shop</h2>
              <h3>$600</h3>
            </div>
            <div className="pricing-table-space" />
            <div className="pricing-table-text">
              <p>
                <strong>The perfect choice for small businesses.</strong>
              </p>
              <p>
                Sell your goods and services from your own online store. Manage
                inventory, schedule sales price durations, and accept all major
                credit cards, Paypal and Apple Pay<span className="asterisk">
                  *
                </span>. Select a <a href="#showSSThemeChoices">template</a> and
                start making money.
              </p>
            </div>
            <div className="pricing-table-features">
              <p>
                <strong>1</strong> MDBC package features
              </p>
              <p>
                <strong>1</strong> Online Store
              </p>
              <p>
                <strong>10</strong> Products added to store
              </p>
              <p>
                <strong>5</strong> Pages (not including store page)
              </p>
              <p>
                <strong>1</strong> Image Correction Session For Product Photos
                (20 max)
                <i
                  data-toggle="tooltip"
                  title="Image Correction includes color correction, web optimization and any client specific cropping, "
                  className="far fa-question-circle"
                />
              </p>
              <p>
                <strong>24/7 Unlimited</strong> Support*
              </p>
              <p>
                <strong>7 - 10 days</strong> Turnaround Time
                <i
                  data-toggle="tooltip"
                  title="Turnaround time is dependent upon the client having all the necessary assets (hi-res images, text for website, email address for contact form, social media accounts)"
                  className="far fa-question-circle"
                />
              </p>
            </div>
            <div className="pricing-table-sign-up">
              {this.renderCheckoutFormMDBC()}
            </div>
          </div>

          {/* <div className="pricing-table">
              <div className="pricing-table-header">
                <h2>Corporate Site</h2>
                <h3>$85/month</h3>
              </div>
              <div className="pricing-table-space" />
              <div className="pricing-table-features">
                <p>
                  <strong>Unlimited</strong> Email Addresses
                </p>
                <p>
                  <strong>85GB</strong> of Storage
                </p>
                <p>
                  <strong>Unlimited</strong> Databases
                </p>
                <p>
                  <strong>50</strong> Domains
                </p>
                <p>
                  <strong>24/7 Unlimited</strong> Support
                </p>
              </div>
              <div className="pricing-table-sign-up">
                <p>
                  <a href="http://bloggingbolt.blogspot.com">Sign Up Now</a>
                </p>
              </div>
            </div> */}
        </div>
      </div>
    );
  }

  render() {
    // console.log(this.props);
    return (
      <div className="container">
        {this.renderIntroSection()}
        {this.renderPricingTables()}
        <div className="disclaimers">
          <p>
            <small>
              *E-commerce services provide by Squarespace. Custom themes are not
              available in The Shop package. A Stripe and/or Paypal account is
              needed to accept payments. A business or online store is needed to
              sell items. For more information on Sqarespace's pricing plans,
              please visit &nbsp;
              <a href="https://www.squarespace.com/pricing#websites">
                Squarespace pricing
              </a>.
            </small>
            <small />
          </p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

// const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SBS);
