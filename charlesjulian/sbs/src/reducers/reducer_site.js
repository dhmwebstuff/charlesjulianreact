export default function() {
  return [
    {
      site: "Charles Julian Personal Site",
      pages: [
        {
          home: [
            {
              content: "<h1>Charles Julian</h1><h1>Web Developer</h1>",
              images: [
                {
                  slide:
                    "/wp-content/themes/charlesjulian/src/images/banner-site.jpg",
                  arrow: "/wp-content/themes/charlesjulian/src/images/arrow.svg"
                }
              ]
            }
          ],
          about: [
            {
              content:
                "<p>Hi! I'm Charles Julian. Welcome to my Portfolio Website!</p></p>I'm a Full-Stack Developer who has a passion for coding and learning new technologies and techniques.</p><p>When I'm not coding, I like to play video games, watch movies and football, and play with my dog Riley.</p>",
              images: [
                {
                  slide: "http://placehold.it/350x150"
                }
              ]
            }
          ],
          portfolio: [
            {
              content:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur quis odio sed ligula laoreet auctor. Pellentesque lobortis quam turpis, in vehicula augue vulputate egestas. Praesent aliquam pretium venenatis. Nunc ac diam eget mi lacinia elementum. Donec congue velit sodales dolor pulvinar, vehicula scelerisque sapien tristique. Nullam condimentum dolor purus, sed malesuada velit venenatis eu. Donec dignissim non est vel tempus.",
              examples: [
                {
                  title: "RELA App",
                  description: "App for Real Estate Lenders Association",
                  featured_image:
                    "/wp-content/themes/charlesjulian/src/images/rela-app-wide.jpg",
                  technologies: [
                    "Cordova",
                    "Javascript",
                    "PHP",
                    "Wordpress",
                    "GIT",
                    "LESS",
                    "Unbuntu"
                  ],
                  link:
                    "https://itunes.apple.com/us/app/rela-real-estate-lenders-association/id1156136789?mt=8"
                },
                {
                  title: "Charles Julian",
                  description: "This Website",
                  featured_image:
                    "/wp-content/themes/charlesjulian/src/images/cjd-site.jpg",
                  technologies: [
                    "React",
                    "Javascript",
                    "Wordpress",
                    "PHP",
                    "LESS",
                    "Unbuntu"
                  ],
                  link: "/"
                },
                {
                  title: "Avenue Of The Americas",
                  description: "Website for Business Association",
                  featured_image:
                    "/wp-content/themes/charlesjulian/src/images/aoa-site.jpg",
                  technologies: [
                    "Javascript",
                    "JQuery",
                    "Wordpress",
                    "PHP",
                    "LESS",
                    "Unbuntu"
                  ],
                  link: "http://aveoftheamericas.org"
                },
                {
                  title: "Memory Game",
                  description:
                    "Card matching game with historical African Americans",
                  featured_image:
                    "/wp-content/themes/charlesjulian/src/images/memory-game.jpg",
                  technologies: ["Javascript", "HTML", "CSS", "Sass"],
                  link: "/wp-content/themes/charlesjulian/portfolio/memoryGame/"
                },
                {
                  title: "Number Guessing Game",
                  description: "Guess a number between 1 and 100",
                  featured_image:
                    "/wp-content/themes/charlesjulian/src/images/guessing-game.jpg",
                  technologies: ["HTML", "CSS", "Javascript", "Less"],
                  link:
                    "/wp-content/themes/charlesjulian/portfolio/numbersgame/numbers_game/"
                }
              ]
            }
          ],
          experience: [
            {
              company: "The Berman Group",
              title: "Web Developer",
              years: "2015 - Present",
              logo: "/wp-content/themes/charlesjulian/src/images/tbg-logo.jpeg",
              responsibilities: [
                "Develop websites to clients specifications using front end and back end technologies.",
                "Develop iOS and Android mobile applications",
                "Create image and html based emails for clients and distribute through email clients such as MailChimp and Constant Contact.",
                "Maintain and update legacy client websites"
              ]
            },
            {
              company: "Agata & Valentina",
              title: "E-Commerce Administrator",
              years: "2014 - 2015",
              logo: "/wp-content/themes/charlesjulian/src/images/av-logo.jpg",
              responsibilities: [
                "Design and program weekly popups for store promotions.",
                "Update front end of e-commerce website with store promotions and weekly specials",
                "Create content for Facebook, Twitter and Instagram accounts."
              ]
            },
            {
              company: "Foundry Lighting",
              title: "Web Master",
              years: "2010 - 2014",
              logo:
                "/wp-content/themes/charlesjulian/src/images/logo-foundrylighting-header.png",
              responsibilities: [
                "Update backend database for e-commerce website",
                "Create html based emails for Website and In-Store promotions",
                "Create content for Facebook, Twitter and Pintrest accounts."
              ]
            }
          ],
          skills: [
            {
              content:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur quis odio sed ligula laoreet auctor. Pellentesque lobortis quam turpis, in vehicula augue vulputate egestas. Praesent aliquam pretium venenatis. Nunc ac diam eget mi lacinia elementum. Donec congue velit sodales dolor pulvinar, vehicula scelerisque sapien tristique. Nullam condimentum dolor purus, sed malesuada velit venenatis eu. Donec dignissim non est vel tempus.",
              examples: [
                {
                  title: "Javascript",
                  image:
                    "/wp-content/themes/charlesjulian/src/images/javascript.jpg"
                },
                {
                  title: "PHP",
                  image: "/wp-content/themes/charlesjulian/src/images/php.jpg"
                },
                {
                  title: "HTML",
                  image: "/wp-content/themes/charlesjulian/src/images/html5.jpg"
                },
                {
                  title: "CSS",
                  image: "/wp-content/themes/charlesjulian/src/images/css.jpg"
                },
                {
                  title: "Less",
                  image: "/wp-content/themes/charlesjulian/src/images/less.jpg"
                },
                {
                  title: "Sass",
                  image: "/wp-content/themes/charlesjulian/src/images/sass.jpg"
                },
                {
                  title: "React",
                  image: "/wp-content/themes/charlesjulian/src/images/react.jpg"
                },
                {
                  title: "JQuery",
                  image:
                    "/wp-content/themes/charlesjulian/src/images/jquery.jpg"
                },
                {
                  title: "Cordova",
                  image:
                    "/wp-content/themes/charlesjulian/src/images/cordova.jpg"
                },
                {
                  title: "Wordpress",
                  image:
                    "/wp-content/themes/charlesjulian/src/images/wordpress.jpg"
                },
                {
                  title: "Unbuntu",
                  image:
                    "/wp-content/themes/charlesjulian/src/images/unbuntu.jpg"
                },
                {
                  title: "Node",
                  image: "/wp-content/themes/charlesjulian/src/images/node.jpg"
                },
                {
                  title: "Git",
                  image: "/wp-content/themes/charlesjulian/src/images/git.jpg"
                },
                {
                  title: "Photoshop",
                  image:
                    "/wp-content/themes/charlesjulian/src/images/photoshop.jpg"
                },
                {
                  title: "Illustrator",
                  image:
                    "/wp-content/themes/charlesjulian/src/images/illustrator.jpg"
                }
              ]
            }
          ],
          contact: [
            {
              content:
                "If you have any questions about my work or would like to hire me for one of your projects, please fill out the form below:"
            }
          ],
          footer: [
            {
              content: "© 2017 Charles Julian"
            }
          ],
          resume: [
            {
              content: "Download my resume",
              link:
                "http://charlesjulianjr.com/wp-content/uploads/2018/01/charles-julian-resume-2017-tech.pdf"
            }
          ]
        }
      ]
    }
  ];
}
