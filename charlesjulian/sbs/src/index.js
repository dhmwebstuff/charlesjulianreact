import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { BrowserRouter, Route } from "react-router-dom";
import { Link } from "react-router";
import PropTypes from "prop-types";

import reducers from "./reducers";
import SBS from "./components/sbs";

const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Route path="/" component={SBS} />
      </div>
    </BrowserRouter>
  </Provider>,
  document.querySelector(".app")
);

// const scrollEmem = function() {
//   const nav = document.getElementsByClassName("nav");
//   var nav_brand = document.getElementsByClassName("navbar-brand");
//   var home_arrow = document.getElementsByClassName("arrow_container");
//
//   for (var i = 0; i < nav[0].children.length; i++) {
//     let nav_links = nav[0].children[i];
//     nav_links.addEventListener("click", function(e) {
//       e.preventDefault();
//
//       var linkDataName = this.children[0].dataset.section;
//       var sections = document.getElementsByTagName("section");
//       for (var j = 0; j < sections.length; j++) {
//         if (linkDataName == sections[j].id) {
//           var section = document.getElementById(sections[j].id);
//           scrollToY(section.offsetTop - 50, 1500, "easeInOutQuint");
//           window.location.hash = this.children[0].getAttribute("href");
//         }
//       }
//     });
//   }
//
//   nav_brand[0].addEventListener("click", function(e) {
//     e.preventDefault();
//     scrollToY(this.offsetTop - 50, 1500, "easeInOutQuint");
//     window.location.hash = this.getAttribute("data-section");
//   });
//
//   home_arrow[0].addEventListener("click", function(e) {
//     scrollToY(
//       document.getElementById("about_section").offsetTop - 50,
//       1500,
//       "easeInOutQuint"
//     );
//   });
// };
//
// window.requestAnimFrame = (function() {
//   return (
//     window.requestAnimationFrame ||
//     window.webkitRequestAnimationFrame ||
//     window.mozRequestAnimationFrame ||
//     function(callback) {
//       window.setTimeout(callback, 1000 / 60);
//     }
//   );
// })();
//
// // main function
// function scrollToY(scrollTargetY, speed, easing) {
//   // scrollTargetY: the target scrollY property of the window
//   // speed: time in pixels per second
//   // easing: easing equation to use
//
//   var scrollY = window.scrollY || document.documentElement.scrollTop,
//     scrollTargetY = scrollTargetY || 0,
//     speed = speed || 2000,
//     easing = easing || "easeOutSine",
//     currentTime = 0;
//
//   // min time .1, max time .8 seconds
//   var time = Math.max(
//     0.1,
//     Math.min(Math.abs(scrollY - scrollTargetY) / speed, 0.8)
//   );
//
//   // easing equations from https://github.com/danro/easing-js/blob/master/easing.js
//   var easingEquations = {
//     easeOutSine: function(pos) {
//       return Math.sin(pos * (Math.PI / 2));
//     },
//     easeInOutSine: function(pos) {
//       return -0.5 * (Math.cos(Math.PI * pos) - 1);
//     },
//     easeInOutQuint: function(pos) {
//       if ((pos /= 0.5) < 1) {
//         return 0.5 * Math.pow(pos, 5);
//       }
//       return 0.5 * (Math.pow(pos - 2, 5) + 2);
//     }
//   };
//
//   // add animation loop
//   function tick() {
//     currentTime += 1 / 60;
//
//     var p = currentTime / time;
//     var t = easingEquations[easing](p);
//
//     if (p < 1) {
//       requestAnimFrame(tick);
//
//       window.scrollTo(0, scrollY + (scrollTargetY - scrollY) * t);
//     } else {
//       window.scrollTo(0, scrollTargetY);
//     }
//   }
//   tick();
// }
//
// scrollEmem();
//
// $(document).on("click", ".navbar-collapse.in", function(e) {
//   if ($(e.target).is("a")) {
//     $(this).collapse("hide");
//   }
// });
//
// // set active nav
//
// var urlHash = window.location.href.split("/")[3];
// $(".nav a").removeClass("active");
// $('.nav > li  > a[href="' + urlHash + '"').addClass("active");
// // console.log(urlHash)
// $(".nav >li > a").on("click", function() {
//   console.log("clicked a tag");
//   $(".nav a").removeClass("active");
//   $(this).addClass("active");
// });
