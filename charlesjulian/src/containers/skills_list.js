import React, { Component } from 'react';
import { connect } from 'react-redux';
import SkillsListItem from '../containers/skills_list_item';


class SkillsList extends Component{
	// renderSkillsList(){
		
	// 	return this.props.skills.map((skill) => {
	// 		return(
	// 			<div key={skill.content} className="skills_content">{skill.content}</div>

	// 		)
	// 	})
	// }

	render(){
		return(
			<section id="skills_section">
				<div className="container">
					<div className="skills_list_wrapper">
						<h2>Skills</h2>
						<SkillsListItem />
					</div>
				</div>
			</section>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		skills: state.site[0].pages[0].skills
	};
}

export default connect(mapStateToProps)(SkillsList);