import React, { Component } from 'react';
import { connect } from 'react-redux';
import ResponsiblityList from '../containers/responsibility_list';
import ExperienceItem from './experience_item';
import Resume from './resume';

class ExperienceList extends Component{
	
	renderExperienceList(){
		
		return this.props.examples.map((example) => {
			const responsibility_views = [];
			example.responsibilities.forEach((value_responsibilities, index_responsibilities) => {
				let this_responsibility = value_responsibilities
				
				responsibility_views.push(<li className='list-group-item' key={index_responsibilities}>{this_responsibility}</li>)
			})
			const logo_url = example.logo;
			const work_logo_image_styles = {
			  backgroundImage: 'url(' + logo_url + ')'
			  
			}

			// return (
			// 	// <ExperienceItem key={example.title} args={...example} />
			// 	<ExperienceItem key={example.title} args={example}/>
			// )

			return(
				<div key={example.title} className="experience_item">
					<h3 className="work_header">
						<span className="work_title">{example.title}</span><span className="work_years badge badge-secondary">{example.years}</span>
					</h3>
					<div className="company_block">
						<h4 className="work_company">{example.company}</h4>
						<div className="responsibilities">
							<ul>
								{responsibility_views}
							</ul>
						</div>
					</div>
				</div>
				
			)
		})
	}

	render(){
		return(
			<section id="experience_section">
				<div className="container">
					<div className="experience_list_wrapper">
						<h2>Experience</h2>
						<Resume />
						{this.renderExperienceList()}
					</div>
				</div>
			</section>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		examples: state.site[0].pages[0].experience

	};
}

export default connect(mapStateToProps)(ExperienceList);