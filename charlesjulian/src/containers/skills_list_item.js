import React, { Component } from 'react';
import { connect } from 'react-redux';

class SkillsListItem extends Component{
	renderSkillsListItem(){
	
		return this.props.skills.map((skill) => {
			return(
				<div key={skill.title} className="skills_item">
					<div className="skill_image">
						<img src={skill.image} alt=""/>
					</div>
					<div className="skill_title">{skill.title}</div>
				</div>
			)
		})
	}

	render(){
		return(
			<div className="skills_item_container">
				{this.renderSkillsListItem()}
			</div>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		skills: state.site[0].pages[0].skills[0].examples
	};
}

export default connect(mapStateToProps)(SkillsListItem);