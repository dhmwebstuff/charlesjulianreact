import React, { Component } from 'react';

import { connect } from 'react-redux';

class Footer extends Component{
	renderFooter(){

		return this.props.home.map((footer) => {
			return(
				

				<div key={footer.content} className="container">

						<div  className="footer_content_block" className="container">
							<div className="footer_content">
								<p>{ footer.content }</p>
							</div>
						</div>
						
					
				</div>


			)
		})
	}

	render(){
		return(
			<footer id="footer" >
				{this.renderFooter()}
			</footer>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		home: state.site[0].pages[0].footer
	};
}

export default connect(mapStateToProps)(Footer);