import React, { Component } from 'react'

class ExperienceItem extends Component{
	render(){
		const { 
			title = '',
			company = '',
			years = ''
		} = this.props.args;
		console.log(this.props.args)
		return(
			<div className="experience_item">
				<div  className="list-group-item">{title}</div>
				<div className="work_company">{company}</div>
				<div className="work_years">{years}</div>
			</div>
		)
	}
}

export default ExperienceItem