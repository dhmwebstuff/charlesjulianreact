import React, { Component } from 'react';

import { connect } from 'react-redux';

class AboutPage extends Component{
	renderAboutPage(){

		return this.props.about.map((about_page) => {
			// var unescaped_content = unescape(about_page.content)
			return(
				

				<div key={about_page.content} className="container">
					<div className="about_wrapper">
						<div  className="about_content_block">
							<div className="about_content">
								<div dangerouslySetInnerHTML={{ __html: about_page.content }}></div>
								
							</div>
						</div>
					</div>
				</div>


				)
		})
	}

	render(){
		return(

			<section id="about_section">
			{this.renderAboutPage()}
			</section>
			)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{

		about: state.site[0].pages[0].about
	};
}

export default connect(mapStateToProps)(AboutPage);