import React, { Component } from 'react';

import { connect } from 'react-redux';

class Footer extends Component{
	renderResume(){

		return this.props.home.map((resume) => {
			return(
				

				<div key={resume.content}>

						<div  className="resume_content_block">
							<a href={resume.link} className="resume_link btn btn-block">
								{ resume.content }
							</a>
						</div>
						
					
				</div>


			)
		})
	}

	render(){
		return(
			<div id="resume" >
				{this.renderResume()}
			</div>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		home: state.site[0].pages[0].resume
	};
}

export default connect(mapStateToProps)(Footer);