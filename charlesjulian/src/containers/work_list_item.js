import React, { Component } from 'react';
import { connect } from 'react-redux';

class WorkList extends Component{
	renderWorkList(){
		return this.props.examples.map((example) => {
			return(
				<div className="portfolio_item">
					<div key={example.title} className="portfolio_title">{example.title}</div>
					<div key={example.description} className="portfolio_title">{example.description}</div>
					<div className="portfolio_image_wrapper">
						<img src={example.featured_image} alt=""/>
					</div>
					
				</div>
				
			)
		})
	}

	render(){
		return(
			<div>
				{this.renderWorkList()}
			</div>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		examples: state.site[0].pages[0].portfolio[0].examples
	};
}

export default connect(mapStateToProps)(WorkList);