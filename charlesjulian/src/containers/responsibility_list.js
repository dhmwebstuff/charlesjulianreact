import React, { Component } from 'react';
import { connect } from 'react-redux';

class ResponsiblityList extends Component{
	renderResponsiblityList(){
		let responsibilities = this.props.responsibilities;
		const responsibility_views = [];
		responsibilities.forEach((value_responsibilities, index_responsibilities) => {
			value_responsibilities.responsibilities.forEach((value_responsibility, index_responsibility)=>{
				
				responsibility_views.push(
					<li key={value_responsibility}>{value_responsibility}</li>
				)
					
				
			})
		})
		return responsibility_views
	}

	render(){
		return(
			<ul className="">
				{this.renderResponsiblityList()}
			</ul>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		responsibilities: state.site[0].pages[0].experience
	};
}

export default connect(mapStateToProps)(ResponsiblityList);
