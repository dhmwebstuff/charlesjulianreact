import React, { Component } from 'react';
import { connect } from 'react-redux';
import ContactForm from '../containers/contact_form';


class ContactPage extends Component{
	renderContactPage(){
		
		return this.props.contact.map((contact_page) => {
			return(
				<div key={contact_page.content} className="contact_content">{contact_page.content}</div>

			)
		})
	}

	render(){
		return(
			<section id="contact_section">

				<div className="container">
					<div className="contact_wrapper">
						<h2>Contact Me</h2>
						{this.renderContactPage()}
						<ContactForm />
					</div>
				</div>
			</section>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		contact: state.site[0].pages[0].contact
	};
}

export default connect(mapStateToProps)(ContactPage);