import React, { Component } from 'react';

import { connect } from 'react-redux';

class HomePage extends Component{
	renderHomePage(){

		return this.props.home.map((homepage) => {
			return(
				

				<div key={homepage.content} className="">
					<div className="home_wrapper">
						<div className="image_wrapper">
							
							<div  className="home_content_block">
								<div className="home_content">
									<div dangerouslySetInnerHTML={{ __html: homepage.content }}></div>
								</div>
							</div>
						</div>
						<div className="arrow_wrapper">
							<div className="arrow_container">
								<img src={homepage.images[0].arrow} alt=""/>
							</div>
							
						</div>
						
						

						</div>
						<div className="media-body">
						
					</div>
				</div>


			)
		})
	}

	render(){
		return(
			<section id="home_section">
				{this.renderHomePage()}
			</section>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		home: state.site[0].pages[0].home
	};
}

export default connect(mapStateToProps)(HomePage);