import React, { Component } from 'react';
import { connect } from 'react-redux';

class PortfolioList extends Component{
	renderWorkList(){
		
		let technology_views = [];
	
		return this.props.examples.map((example) => {
			const image_url = example.featured_image;
			const portfolio_image_styles = {
			  backgroundImage: 'url(' + image_url + ')'
			  
			}
			let technology_views = [];
			example.technologies.forEach((value_technology, index_technology)=>{
				technology_views.push(
					<li key={value_technology + '-' + (index_technology * Math.random())}>{value_technology}</li>
					)
			})
			return(
				<a  key={example.title} href={example.link} className="portfolio_item" target="_blank">
					<div className="portfolio_block">
						<div className="portfolio_info_block">
							<div className="portfolio_title">{example.title}</div>
							<div key={example.description} className="portfolio_description">{example.description}</div>
						</div>
						<div className="portfolio_image_wrapper">
							<div className="image_wrapper">
								<div className="image_block" style={portfolio_image_styles}></div>
							</div>
							<div className="technology_wrapper">
								<h4>Technologies Used:</h4>
								<ul>
									{technology_views}
								</ul>
							</div>
						</div>
					</div></a>
				
			)
		})
	}

	render(){
		return(
			<section id="portfolio_section">
				<div className="container">
					<div className="portfolio_wrapper_block">
						<h2>Portfolio</h2>
						<div className="portfolio_wrapper">
							{this.renderWorkList()}
						</div>
					</div>
				</div>
			</section>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		examples: state.site[0].pages[0].portfolio[0].examples
	};
}

export default connect(mapStateToProps)(PortfolioList);