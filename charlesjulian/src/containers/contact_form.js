import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { processForm } from '../actions'

class ContactForm extends Component {
	renderField(field){

		const { meta: { touched, error } }	= field;
		const className = `form-group ${touched && error ? 'has-error' : ''}`;
		return (
			<div className={className}>
				<label>{field.label}</label>
				<input 
					className="form-control"
					type={field.type}
					{...field.input}
				/>
				
				<div className="text-danger">{touched ? error : ''}</div>
			</div>
		)
	}

	renderTextarea(field) {
		const { meta: { touched, error } }	= field;
		const className = `form-group ${touched && error ? 'has-error' : ''}`;
	  return (
	    <div className="form-group">
	      <label>{field.label}</label>
	      <textarea className="form-control" {...field.input} rows="7" />
	      <div className="text-danger">{touched ? error : ''}</div>
	    </div>
	  )
	}

	onSubmit(values){
		// this === component
		this.props.processForm(values);
		
	}


	render(){

		const { handleSubmit } = this.props;
		return (
			<div className="form_wrapper">
				<div className="comfirmation_msg"></div>
				<form id="contact_form" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
					<Field
						label="Name" 
						name="name"
						type="text"
						component={this.renderField}
					/>
					<Field 
						label="Email"
						name="email"
						type="email"
						component={this.renderField}
					/>
					<Field 
						label="Message"
						name="message"
						component={this.renderTextarea}
					/>
					<button type="submit" className="btn btn-primary">Submit</button>
				</form>
			</div>	
			
		)
	}
}


function validate(values){
	const errors = {};

	// validate the inputs
	if (!values.name) {
		errors.name = "Please enter you name";
	}

	if (!values.email) {
		errors.email = "Please enter an email address";
	}

	if (!values.message) {
		errors.message = "Please enter a message";
	}


	// if errors is empty form is ok to submit
	// if errors has any properties form is invalid
	return errors;
}


export default reduxForm({
	validate,
	form: 'PostsContactForm'
})
(
	connect(null, { processForm })(ContactForm)
)