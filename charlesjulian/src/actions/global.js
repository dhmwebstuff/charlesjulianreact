export const CLEAR_DOM = "CLEAR_DOM";

/* REDUCER */
const initialState = {
  clearDOM: false
};

export function global(state = initialState, action) {
  // console.log(action);

  switch (action.type) {
    case CLEAR_DOM:
      console.log("Clear dom");

      return { state, clearDOM: action.clearDOM };

    default:
      return state;
  }
}

export default global;

/* ACTION CREATORS */

export function clearDOM() {
  return {
    type: CLEAR_DOM,
    clearDOM: true
  };
}
