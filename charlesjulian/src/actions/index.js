import axios from 'axios';

const PROCESS_FORM_URL = '../wp-content/themes/charlesjulian/processForm.php';

export const FORM_RESPONSE = 'form_response';

export function processForm(values){
	const request = axios.post(`${PROCESS_FORM_URL}`, values).then(function(response){
		
		if (response.data == 'success') {
			const success_msg = 'Thank you. Your message been sent.'
			
			var input_fields = document.getElementsByTagName('input');
			var text_message = document.getElementsByTagName('textarea')

			// reset form
			for(var i = 0; i < input_fields.length; i++){
				input_fields[i].value = "";
			}
			text_message.message.value= "";
			
			
			document.getElementsByClassName('comfirmation_msg')[0].classList.remove('displayNone');
			document.getElementsByClassName('comfirmation_msg')[0].innerHTML = success_msg;
			document.getElementsByClassName('comfirmation_msg')[0].classList.add('text-success');
			document.getElementsByClassName('comfirmation_msg')[0].classList.add('alert');
			document.getElementsByClassName('comfirmation_msg')[0].classList.add('alert-success');


			function hideConfirmationMessage(){
				setTimeout(function(){
					
					document.getElementsByClassName('comfirmation_msg')[0].classList.add('displayNone');

				}, 5000)
			}
			 
			hideConfirmationMessage();
			
			  
			
		} else {
			const fail_msg = 'There was an error sending your message. Please try again.'
			
			
		}
	})
	
	return {
		type: FORM_RESPONSE,
		payload: request
	}
}