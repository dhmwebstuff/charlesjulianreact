import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { global, clearDOM } from "../actions/global";
import { connect } from "react-redux";

class SBS extends Component {
  constructor(props) {
    super(props);

    this.state = {
      clearDOM: false
    };
  }

  componentDidMount() {
    this.clearCJDOM();
    console.log("REACT");
  }

  clearCJDOM() {
    let appWrapper = document.querySelector(".app-wrapper");
    appWrapper.style = "display: none;";
    console.log("IN CCJD", appWrapper);
    console.log("CLEAR CJ DOM");
    console.log("REACT")
  }
  renderIntroSection() {
    return (
      <div className="intro">
        <h1>Small Business Solutions</h1>
        <p>
          A website is vital to entrepreneurs and small business owners in
          today's economy. E-commerce platforms offer an easier and more
          affordable way to sell your goods or services without the hassle of
          worrying about security and customer support..
        </p>
        <h2>Small Business Packages</h2>
      </div>
    );
  }

  renderPricingTables() {
    return (
      <div className="page-container">
        <div className="pricing-table-wrapper">
          <div className="pricing-table">
            <div className="pricing-table-header">
              <h2>The MDBC</h2>
              <h3>$200</h3>
            </div>
            <div className="pricing-table-space" />
            <div className="pricing-table-text">
              <p>
                <strong>The Modern Day Business Card.</strong>
              </p>
              <p>
                A solution for the starting entrepreneur. Your website will give
                a concise vision of your buisness while also giving options for
                client leads and feedback.
              </p>
            </div>
            <div className="pricing-table-features">
              <p>
                <strong>1</strong> Single Page Website
              </p>
              <p>
                <strong>1</strong> Image Slideshow/Video Loop
              </p>
              <p>
                <strong>1</strong> Bio Section
              </p>
              <p>
                <strong>1</strong> Socia Media Integration (3 post from Twitter, Facebook, or Instagram)
              </p>
              <p>
                <strong>1</strong> Services/Features Section
              </p>
              <p>
                <strong>1</strong> Newsletter Signup Form To Your Email Campaign
                Provider
              </p>
              <p>
                <strong>1</strong> Contact Form
              </p>
              <p>
                <strong>7 days</strong> Turnaround Time
                <i
                  data-toggle="tooltip"
                  title="Turnaround time is dependent upon the client having all the necessary assets (hi-res images, text for website, email address for contact form, social media accounts)"
                  className="far fa-question-circle"
                />
              </p>
              <a href="#" className="example-link">
                example
              </a>
            </div>
            <div className="pricing-table-sign-up">
              <p>
                <a href="http://bloggingbolt.blogspot.com">Sign Up Now</a>
              </p>
            </div>
          </div>

          <div className="pricing-table pricing-table-highlighted">
            <div className="pricing-table-header">
              <h2>The Shop</h2>
              <h3>$500</h3>
            </div>
            <div className="pricing-table-space" />
            <div className="pricing-table-text">
              <p>
                <strong>The perfect choice for small businesses.</strong>
              </p>
              <p>
                Sell your goods and services from your own online store. Manage
                inventory, schedule sales price durations, and accept all major
                credit cards, Paypal and Apple Pay<span className="asterisk">
                  *
                </span>. Select a <a href="#showSSThemeChoices">template</a> and
                start making money.
              </p>
            </div>
            <div className="pricing-table-features">
              <p>
                <strong>1</strong> MDBC package features
              </p>
              <p>
                <strong>1</strong> Online Store
              </p>
              <p>
                <strong>10</strong> Products added to store
              </p>
              <p>
                <strong>5</strong> Pages (not including store page)
              </p>
              <p>
                <strong>1</strong> Image Correction Session For Product Photos
                (20 max){" "}
                <i
                  data-toggle="tooltip"
                  title="Image Correction includes color correction, web optimization and any client specific cropping, "
                  className="far fa-question-circle"
                />
              </p>
              <p>
                <strong>24/7 Unlimited</strong> Support*
              </p>
            </div>
            <div className="pricing-table-sign-up">
              <p>
                <a href="http://bloggingbolt.blogspot.com">Sign Up Now</a>
              </p>
            </div>
          </div>

          <div className="pricing-table">
            <div className="pricing-table-header">
              <h2>Corporate Site</h2>
              <h3>$85/month</h3>
            </div>
            <div className="pricing-table-space" />
            <div className="pricing-table-features">
              <p>
                <strong>Unlimited</strong> Email Addresses
              </p>
              <p>
                <strong>85GB</strong> of Storage
              </p>
              <p>
                <strong>Unlimited</strong> Databases
              </p>
              <p>
                <strong>50</strong> Domains
              </p>
              <p>
                <strong>24/7 Unlimited</strong> Support
              </p>
            </div>
            <div className="pricing-table-sign-up">
              <p>
                <a href="http://bloggingbolt.blogspot.com">Sign Up Now</a>
              </p>
            </div>
          </div>
        </div>
        <div className="disclaimers">
          <p>
            <small>
              *E-commerce services provide by Squarespace. Custom themes are not
              available in The Shop package. A Stripe and/or Paypal account is
              needed to accept payments. A business or online store is needed to
              sell items. For more information on Sqarespace's pricing plans,
              please visit &nbsp;
              <a href="https://www.squarespace.com/pricing#websites">
                Squarespace pricing
              </a>.
            </small>
          </p>
        </div>
      </div>
    );
  }

  render() {
    // console.log(this.props);
    return (
      <div className="container">
        {this.renderIntroSection()}
        {this.renderPricingTables()}
      </div>
    );
  }
}

const mapStateToProps = state => ({});

// const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SBS);
