import React, { Component } from 'react';

import { connect } from 'react-redux';

class PageHome extends Component{
	renderWorkList(){
		return this.props.examples.map((example) => {
			return(
				<div key={example.title} className="list-group-item">{example.title}</div>
			)
		})
	}

	render(){
		return(
			<div>
				{this.renderPageHome()}
			</div>
		)
	}
}

function mapStateToProps(state){
	console.log(state.site[0].pages[0].portfolio[0].examples)
	// whatever is returned will show up as props 
	return{
		examples: state.site[0].pages[0].portfolio[0].examples
	};
}

export default connect(mapStateToProps)(WorkList);