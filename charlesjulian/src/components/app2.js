import React, { Component } from "react";
import PropTypes from "prop-types";
import Navbar from "../components/nav_bar";
import HomePage from "../containers/home_page";
import AboutPage from "../containers/about_page";
import PortfolioContent from "../containers/portfolio_section";
import PortfolioListItem from "../containers/portfolio_list_item";
import BookList from "../containers/book-list";
import ExperienceList from "../containers/experience_list";
import SkillsList from "../containers/skills_list";
import ContactPage from "../containers/contact_page";
import Footer from "../containers/footer";
import SBS from "../components/sbs";

export default class App extends Component {
  render() {
    return (
      <div className="app-wrapper">
        <Navbar />
        <HomePage />
        <AboutPage />
        <PortfolioListItem />
        <ExperienceList />
        <SkillsList />
        <ContactPage />
        <Footer />
      </div>
    );
  }
}
