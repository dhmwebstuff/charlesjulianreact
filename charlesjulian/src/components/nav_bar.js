import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-default  navbar-fixed-top">
          <div className="menu-container">
            <div className="container">
              <div className="navbar-header">
                <button
                  type="button"
                  className="navbar-toggle collapsed"
                  data-toggle="collapse"
                  data-target="#bs-example-navbar-collapse-1"
                  aria-expanded="false"
                >
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                </button>
                <a className="navbar-brand" href="#home" data-section="home">
                  Charles Julian - Web Developer
                </a>
              </div>

              <div
                className="collapse navbar-collapse"
                id="bs-example-navbar-collapse-1"
              >
                <ul className="nav navbar-nav pull-right">
                  <li>
                    <a href="#about" data-section="about_section">
                      About
                    </a>
                  </li>
                  <li>
                    <a href="#portfolio" data-section="portfolio_section">
                      Portfolio
                    </a>
                  </li>
                  <li>
                    <a href="#experience" data-section="experience_section">
                      Experience
                    </a>
                  </li>
                  <li>
                    <a href="#skills" data-section="skills_section">
                      Skills
                    </a>
                  </li>
                  <li>
                    <a href="#contact" data-section="contact_section">
                      Contact
                    </a>
                  </li>
                  <li>
                    <Link to="/sbs">Small Biz Solutions</Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
