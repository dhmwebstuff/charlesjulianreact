import React, { Component } from 'react';

import { connect } from 'react-redux';

class PortfolioPage extends Component{
	renderPortfolioPage(){

		return this.props.portfolio.map((portfolioPage) => {
			return(

				<section key={portfolioPage.content}>
					<div className="portfolio_wrapper">
						<div  className="portfolio_content_block">{portfolioPage.content}</div>
					</div>
						
				</section>


			)
		})
	}

	render(){
		return(
			<section className="portfilio_section">
				{this.renderPortfolioPage()}
			</section>
		)
	}
}

function mapStateToProps(state){
	
	// whatever is returned will show up as props 
	return{
		portfolio: state.site[0].pages[0].portfolio
	};
}

export default connect(mapStateToProps)(PortfolioPage);