import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import SiteReducer from './reducer_site';



const rootReducer = combineReducers({
  site: SiteReducer,
  form: formReducer
});

export default rootReducer;
